package tdd.training.mra;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


public class MarsRover {
	
	public static final String initialLandingPosition="("+0+","+0+",N)";
	protected String landingPosition;
	protected int planetX,planetY;
	protected List<String> planetObstacles=new ArrayList<String>();
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX=planetX;
		this.planetY=planetY;
		this.planetObstacles=planetObstacles;
		this.landingPosition=initialLandingPosition;
	}
	
	public void setPosition(int x,int y,String direction) {
		this.landingPosition="("+x+","+y+","+direction+")";
	}
	
	public String getPosition() {
		return this.landingPosition;
	}
	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		
		for(int i=0;i<planetObstacles.size();i++) {
			String obstacle="("+x+","+y+")";
			if(planetObstacles.get(i).equals(obstacle)) {
				return true;
			}
		}
		return false;
	}
	
	
	public String getObstacleAt(int x, int y) {
		for(int i=0;i<planetObstacles.size();i++) {
			String obstacle="("+x+","+y+")";
			if(planetObstacles.get(i).equals(obstacle)) {
				return obstacle;
			}
		}
		return null;
	}
	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		int count=0;
		Set<String> obstacles = new LinkedHashSet<String>();
		String direction=null;
		for(int i=0;i<commandString.length();i++) {
			switch (commandString.charAt(i)) {
				case 'f':
					int firstF=Integer.parseInt(this.landingPosition.substring(1,2)); 
					int secondF=Integer.parseInt(this.landingPosition.substring(3,4));
				direction=this.landingPosition.substring(5,6);
				if(secondF==this.planetY-1 && direction.equals("N")) {
					secondF=0;
					landingPosition="("+firstF+","+secondF+","+direction+")";
					if(planetContainsObstacleAt(firstF,secondF)) {
						obstacles.add(getObstacleAt(firstF,secondF));
						landingPosition="("+firstF+","+(this.planetY-1)+","+direction+")";
						count++;
					}
				}else if(firstF==this.planetY-1 && direction.equals("E")) {
					firstF=0;
					landingPosition="("+firstF+","+secondF+","+direction+")";
					if(planetContainsObstacleAt(firstF,secondF)) {
						obstacles.add(getObstacleAt(firstF,secondF));
						landingPosition="("+(this.planetY-1)+","+secondF+","+direction+")";
						count++;
					}
				}else if(firstF==0 && direction.equals("W")) {
					firstF=this.planetX-1;
					landingPosition="("+firstF+","+secondF+","+direction+")";
					if(planetContainsObstacleAt(firstF,secondF)) {
						obstacles.add(getObstacleAt(firstF,secondF));
						landingPosition="("+0+","+secondF+","+direction+")";
						count++;
					}
				}else if(secondF==0 && direction.equals("S")){
					secondF=this.planetY-1;
					landingPosition="("+firstF+","+secondF+","+direction+")";
					if(planetContainsObstacleAt(firstF,secondF)) {
						obstacles.add(getObstacleAt(firstF,secondF));
						landingPosition="("+firstF+","+0+","+direction+")";
						count++;
					}
				}else if(direction.equals("N")) {
					landingPosition="("+firstF+","+(++secondF)+","+direction+")";
					if(planetContainsObstacleAt(firstF,secondF)) {
						obstacles.add(getObstacleAt(firstF,secondF));
						landingPosition="("+firstF+","+(--secondF)+","+direction+")";
						count++;
					}
				}else if (direction.equals("E")) {
					landingPosition="("+(++firstF)+","+secondF+","+direction+")";
					if(planetContainsObstacleAt(firstF,secondF)) {
						obstacles.add(getObstacleAt(firstF,secondF));
						landingPosition="("+(--firstF)+","+secondF+","+direction+")";
						count++;
					}
				}else if(direction.equals("S")){
					landingPosition="("+firstF+","+(--secondF)+","+direction+")";
					if(planetContainsObstacleAt(firstF,secondF)) {
						obstacles.add(getObstacleAt(firstF,secondF));
						landingPosition="("+firstF+","+(++secondF)+","+direction+")";
						count++;
					}
				}else {
					landingPosition="("+(--firstF)+","+secondF+","+direction+")";
					if(planetContainsObstacleAt(firstF,secondF)) {
						obstacles.add(getObstacleAt(firstF,secondF));
						landingPosition="("+(++firstF)+","+secondF+","+direction+")";
						count++;
					}
				}
					break;
				case 'b':
					int firstB=Integer.parseInt(this.landingPosition.substring(1,2)); 
					int secondB=Integer.parseInt(this.landingPosition.substring(3,4));
					direction=this.landingPosition.substring(5,6);
					if(secondB==0 && direction.equals("N")) {
						secondB=--this.planetY;
						landingPosition="("+firstB+","+secondB+","+direction+")";
						if(planetContainsObstacleAt(firstB,secondB)) {
							obstacles.add(getObstacleAt(firstB,secondB));
							landingPosition="("+firstB+","+0+","+direction+")";
							count++;
						}
					}else if(secondB==this.planetY-1 && direction.equals("S")) {
						secondB=0;
						landingPosition="("+firstB+","+secondB+","+direction+")";
						if(planetContainsObstacleAt(firstB,secondB)) {
							obstacles.add(getObstacleAt(firstB,secondB));
							landingPosition="("+firstB+","+(this.planetY-1)+","+direction+")";
							count++;
						}
					}else if(firstB==0 && direction.equals("E")){
						firstB=--this.planetX;
						landingPosition="("+firstB+","+secondB+","+direction+")";
						if(planetContainsObstacleAt(firstB,secondB)) {
							obstacles.add(getObstacleAt(firstB,secondB));
							landingPosition="("+0+","+secondB+","+direction+")";
							count++;
						}
					}else if(firstB==this.planetX-1 && direction.equals("W")){
						firstB=0;
						landingPosition="("+firstB+","+secondB+","+direction+")";
						if(planetContainsObstacleAt(firstB,secondB)) {
							obstacles.add(getObstacleAt(firstB,secondB));
							landingPosition="("+(this.planetX-1)+","+secondB+","+direction+")";
							count++;
						}
					}else if(direction.equals("N")) {
						landingPosition="("+firstB+","+(--secondB)+","+direction+")";
						if(planetContainsObstacleAt(firstB,secondB)) {
							obstacles.add(getObstacleAt(firstB,secondB));
							landingPosition="("+firstB+","+(++secondB)+","+direction+")";
							count++;
						}
					}else if (direction.equals("E")) {
						landingPosition="("+(--firstB)+","+secondB+","+direction+")";
						if(planetContainsObstacleAt(firstB,secondB)) {
							obstacles.add(getObstacleAt(firstB,secondB));
							landingPosition="("+(++firstB)+","+secondB+","+direction+")";
							count++;
						}
					}else if(direction.equals("S")){
						landingPosition="("+firstB+","+(++secondB)+","+direction+")";
						if(planetContainsObstacleAt(firstB,secondB)) {
							obstacles.add(getObstacleAt(firstB,secondB));
							landingPosition="("+firstB+","+(--secondB)+","+direction+")";
							count++;
						}
					}else {
						landingPosition="("+(++firstB)+","+secondB+","+direction+")";
						if(planetContainsObstacleAt(firstB,secondB)) {
							obstacles.add(getObstacleAt(firstB,secondB));
							landingPosition="("+(--firstB)+","+secondB+","+direction+")";
							count++;
						}
					}
					break;
				case 'l':
					
					String firstL=this.landingPosition.substring(1,2); 
					String secondL=this.landingPosition.substring(3,4);
					direction=this.landingPosition.substring(5,6);
					if(direction.equals("N")) {
						landingPosition="("+firstL+","+secondL+","+"W)";
					}else if(direction.equals("E")) {
						landingPosition="("+firstL+","+secondL+","+"N)";
					}else if(direction.equals("S")) {
						landingPosition="("+firstL+","+secondL+","+"E)";
					}else {
						landingPosition="("+firstL+","+secondL+","+"S)";
					}
					
					break;
				case 'r':
					
					String firstR=this.landingPosition.substring(1,2); 
					String secondR=this.landingPosition.substring(3,4);
					direction=this.landingPosition.substring(5,6);
					if(direction.equals("N")) {
						landingPosition="("+firstR+","+secondR+","+"E)";
					}else if(direction.equals("E")) {
						landingPosition="("+firstR+","+secondR+","+"S)";
					}else if(direction.equals("S")) {
						landingPosition="("+firstR+","+secondR+","+"W)";
					}else {
						landingPosition="("+firstR+","+secondR+","+"N)";
					}
					
					break;
				default:
					landingPosition=initialLandingPosition;
			}
		}
		
		if(count!=0) {
			int first=Integer.parseInt(this.landingPosition.substring(1,2)); 
			int second=Integer.parseInt(this.landingPosition.substring(3,4));
			direction=this.landingPosition.substring(5,6);
			landingPosition="("+first+","+second+","+direction+")";
			Iterator<String> value = obstacles.iterator();
			while(value.hasNext()) {
				landingPosition=landingPosition+value.next();
			}
			
		}
		return landingPosition;	
	}
}
