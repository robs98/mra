package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	//Test user story 1
	public void testPlanet() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover rover=new MarsRover(10,10,planetObstacles);
		assertTrue(rover.planetContainsObstacleAt(5,5));
	}
	
	@Test
	//Test user story 2
	
	public void testLanding() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover rover=new MarsRover(10,10,planetObstacles);
		String command=" ";
		assertEquals("(0,0,N)",rover.executeCommand(command));
	}
	
	
	@Test
	//Test user story 3
	
	public void testTurningLeft() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover rover=new MarsRover(10,10,planetObstacles);
		String command="l";
		assertEquals("(0,0,W)",rover.executeCommand(command));
	}
	
	@Test
	//Test user story 3
	
	public void testTurningRight() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover rover=new MarsRover(10,10,planetObstacles);
		String command="r";
		assertEquals("(0,0,E)",rover.executeCommand(command));
	}
	
	@Test
	//Test user story 4
	
	public void testTMovingForward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover rover=new MarsRover(10,10,planetObstacles);
		rover.setPosition(7, 6, "N");
		String command="f";
		assertEquals("(7,7,N)",rover.executeCommand(command));
	}
	
	@Test
	//Test user story 5
	
	public void testMovingBackward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover rover=new MarsRover(10,10,planetObstacles);
		rover.setPosition(5, 8, "E");
		String command="b";
		assertEquals("(4,8,E)",rover.executeCommand(command));
	}
	
	@Test
	//Test user story 6
	
	public void testMovingCombined() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover rover=new MarsRover(10,10,planetObstacles);
		String command="ffrff";
		assertEquals("(2,2,E)",rover.executeCommand(command));
	}
	
	@Test
	//Test user story 7
	
	public void testWrapping() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover rover=new MarsRover(10,10,planetObstacles);
		String command="b";
		assertEquals("(0,9,N)",rover.executeCommand(command));
	}
	
	@Test
	//Test user story 8
	
	public void testSingleObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		MarsRover rover=new MarsRover(10,10,planetObstacles);
		String command="ffrff";
		assertEquals("(1,2,E)(2,2)",rover.executeCommand(command));
	}
	
	@Test
	//Test user story 9
	
	public void testMultipleObstacles() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		MarsRover rover=new MarsRover(10,10,planetObstacles);
		String command="ffrfffrflf";
		assertEquals("(1,1,E)(2,2)(2,1)",rover.executeCommand(command));
		
	}
	@Test
	
	//Test user story 10
	public void testWrappingObjects() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,9)");
		MarsRover rover=new MarsRover(10,10,planetObstacles);
		String command="b";
		assertEquals("(0,0,N)(0,9)",rover.executeCommand(command));
	}
	
@Test
	
	//Test user story 11
/*
 *  On a 6x6 planet, there are obstacles at coordinates (2,2), (0,5), and (5,0).
 *   A rover with status “(0,0,N)”, after executing the command string “ffrfffrbbblllfrfrbbl”, 
 *   returns a string containing its new status and the encountered obstacles, namely: “(0,0,N)(2,2)(0,5)(5,0)”.

 */
	public void testTour() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
		MarsRover rover=new MarsRover(6,6,planetObstacles);
		String command="ffrfffrbbblllfrfrbbl";
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)",rover.executeCommand(command));
	}
	
}
